import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TodosModule } from './todos/todos.module';
import {MongooseModule} from '@nestjs/mongoose';

@Module({
  imports: [TodosModule,
    MongooseModule.forRoot('mongodb+srv://jaideep:Deepu@123@cluster0.0oz68.mongodb.net/nest?retryWrites=true&w=majority', { useNewUrlParser: true },
    )
],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
