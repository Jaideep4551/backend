import { Document } from 'mongoose';

export interface Todos extends Document {
  title: String,
  description: String,
  
}