import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Todos } from './interfaces/todos.interface';
import { CreateTodosDTO } from './dto/create-todos.dto';


@Injectable()
export class TodosService {
    constructor(@InjectModel('Todos') private readonly TodosModel: Model<Todos>) { }

    // fetch all Todoss
    async getAllTodoss(): Promise<Todos[]> {
      const Todoss = await this.TodosModel.find().exec();
      return Todoss;
    }

    // Get a single Todos
    async getTodos(TodosID): Promise<Todos> {
      const Todos = await this.TodosModel.findById(TodosID).exec();
      return Todos;
    }

    // post a single Todos
    async addTodos(createTodosDTO: CreateTodosDTO): Promise<Todos> {
      const newTodos = await new this.TodosModel(createTodosDTO);
      return newTodos.save();
    }

    // Edit Todos details
    async updateTodos(TodosID, createTodosDTO: CreateTodosDTO): Promise<Todos> {
      const updatedTodos = await this.TodosModel
          .findByIdAndUpdate(TodosID, createTodosDTO, { new: true });
      return updatedTodos;
    }

    // Delete a Todos
    async deleteTodos(TodosID): Promise<any> {
      const deletedTodos = await this.TodosModel.findByIdAndRemove(TodosID);
      return deletedTodos;
    }

}