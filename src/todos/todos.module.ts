import { Module, NestModule, MiddlewareConsumer , RequestMethod} from '@nestjs/common';
import { TodosController } from './todos.controller';
import { TodosService } from './todos.service';
import { MongooseModule } from '@nestjs/mongoose';
import { TodosSchema } from './schemas/todos.schema';
import {AuthenticationMiddleware} from 'src/common/authentication.middleware';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Todos', schema: TodosSchema }])
  ],
  controllers: [TodosController],
  providers: [TodosService]
})
export class TodosModule implements NestModule {
  configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
    consumer.apply(AuthenticationMiddleware).forRoutes(
      { method: RequestMethod.POST, path: '/todos/create' },
      { method: RequestMethod.PUT, path: '/todos/update' },
      { method: RequestMethod.DELETE, path: '/todos/delete' }
    )
  }
 }