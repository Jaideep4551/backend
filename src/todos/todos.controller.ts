import { Controller, Post, Res, Body, HttpStatus, Get, Param, NotFoundException, Put, Query, Delete } from '@nestjs/common';
import { TodosService } from './todos.service';
import { CreateTodosDTO } from './dto/create-todos.dto';
import { response } from 'express';

@Controller('Todos')
export class TodosController {
    constructor(private TodosService: TodosService) { }

    // add a Todos
    @Post('/create')
    async addTodos(@Res() res,@Body() createTodosDTO: CreateTodosDTO) {
        const Todos = await this.TodosService.addTodos(createTodosDTO);
        return res.status(HttpStatus.OK).json({
            message: 'Todo Added Successfully!',
            todos:Todos,
        });
    }

    // get all todos

    @Get('todos')
    async getAllTodos(@Res() response) {
        const todos = await this.TodosService.getAllTodoss();
        return response.status(HttpStatus.OK).json(todos);
    }

    // Fetch a particular Todos using ID
    @Get('/:TodosID')
    async getTodos(@Res() res,@Param('TodosID') TodosID) {
        const Todos = await this.TodosService.getTodos(TodosID);
        if (!Todos) throw new NotFoundException('Todos does not exist!');
        return res.status(HttpStatus.OK).json(Todos);
    }

    // Update a Todos's details
    @Put('/update/:TodosID')
    async updateTodos(@Res() res, @Param('TodosID') TodosID, @Body() createTodosDTO: CreateTodosDTO) {
        const Todos = await this.TodosService.updateTodos(TodosID, createTodosDTO);
        if (!Todos) throw new NotFoundException('Todos does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Todo has been Successfully Updated',
            todos:Todos,
        });
    }
    
    // Delete a Todos
    @Delete('/delete/:TodosID')
    async deleteTodos(@Res() res, @Param('TodosID') TodosID) {
        const Todos = await this.TodosService.deleteTodos(TodosID);
        if (!Todos) throw new NotFoundException('Todos does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'Todo has been Successfully Deleted',
            todos:Todos,
        });
    }
}